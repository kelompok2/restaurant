package com.kelompok2.kasir.data;

import android.net.Uri;

/**
 * Created by user on 4/5/2016.
 */
public class Transaksi {
    private String idMerchant = "merchant";
    private String idUser;
    private String idOrder;
    private String idMeja;
    private String type;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getIdUser() {
        return idUser;
    }

    public void setIdUser(String idUser) {
        this.idUser = idUser;
    }

    public String getIdOrder() {
        return idOrder;
    }

    public void setIdOrder(String idOrder) {
        this.idOrder = idOrder;
    }

    public String getIdMeja() {
        return idMeja;
    }

    public void setIdMeja(String idMeja) {
        this.idMeja = idMeja;
    }
// wait

    @Override
    public String toString() {
        String result;
        result = Uri.encode("id_merchant=" + idMerchant +
                "&id_order=" + idOrder +
                "&id_user=" + idUser +
                "&type=" + type +
                "&id_meja=" + idMeja);
        result=result.replaceAll("%20","+");
        result=result.replaceAll("%3D","=");
        result=result.replaceAll("%26","&");
        return result;
    }
}
