package com.kelompok2.kasir.data;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Chinta Kumala on 04/04/2016.
 */
public class ProfileHelper {
    public static final String ID_KASIR = "id_kasir";

    private SharedPreferences prefs;
    Context context;
    private static ProfileHelper helper;

    public static ProfileHelper getInstance(Context context) {
        if (helper == null) {
            helper = new ProfileHelper(context);
        }
        return helper;
    }

    private ProfileHelper(Context context) {
        this.context = context;
        prefs = context.getSharedPreferences("ProfileHelper", 0);
    }

    public void setIdKasir(String idKasir) {
        if (prefs != null) {
            SharedPreferences.Editor editor = prefs.edit();
            editor.putString(ID_KASIR, idKasir);
            editor.apply();
        }
    }

    public String getIdKasir() {
        if (prefs != null) {
            return prefs.getString(ID_KASIR, null);
        }
        return null;
    }
}
