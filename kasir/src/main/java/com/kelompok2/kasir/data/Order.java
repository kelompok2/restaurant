package com.kelompok2.kasir.data;

import android.net.Uri;

import java.io.Serializable;

/**
 * Created by user on 4/5/2016.
 */
public class Order implements Serializable{
    private String idMenu;
    private String jumlah;
    private String status;

    public String getIdMenu() {
        return idMenu;
    }

    public void setIdMenu(String idMenu) {
        this.idMenu = idMenu;
    }

    public String getJumlah() {
        return jumlah;
    }

    public void setJumlah(String jumlah) {
        this.jumlah = jumlah;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
