package com.kelompok2.kasir.koneksi;

/**
 * Created by User on 3/28/2016.
 */
public class Filter {
    public static final String LOGIN = "merchantLogin";
    public static final String GET_ORDER = "merchantGetOrder";
    public static final String TRANSAKSI = "merchantTransaction";
    public static final String TOP_UP = "merchantTopup";
}
