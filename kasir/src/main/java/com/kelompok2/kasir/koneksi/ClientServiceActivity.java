package com.kelompok2.kasir.koneksi;

import com.restaurant.connection.Service;
import com.restaurant.connection.ServiceActivity;

/**
 * Created by User on 3/28/2016.
 */
public class ClientServiceActivity extends ServiceActivity {
    public void login(String parameter) {
        requestPost(parameter, Path.LOGIN, Filter.LOGIN);
    }

    public void getOrder(String parameter) {
        requestPost(parameter, Path.GET_ORDER, Filter.GET_ORDER);
    }

    public void transaksi(String parameter) {
        requestPost(parameter, Path.TRANSAKSI, Filter.TRANSAKSI);
    }

    public void topup(String parameter) {
        requestPost(parameter, Path.TOPUP, Filter.TOP_UP);
    }

    private void requestPost(String parameter, String path, String filter) {
        sendPostRequest(Path.BASE_URL + path, parameter, filter, Service.CONTENT_TYPE_URLENCODED);
    }

    private void requestGet(String path, String filter) {
        sendGetRequest(Path.BASE_URL + path, filter);
    }
}
