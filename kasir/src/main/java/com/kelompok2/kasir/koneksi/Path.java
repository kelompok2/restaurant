package com.kelompok2.kasir.koneksi;

/**
 * Created by User on 3/28/2016.
 */
public class Path {
    public static final String BASE_URL = "http://www.tubes.pe.hu/api/";

    public static final String LOGIN = "merchant/login.php";
    public static final String GET_ORDER = "merchant/cariorder.php";
    public static final String TRANSAKSI = "customer/transaksi.php";
    public static final String TOPUP = "merchant/topup.php";
}
