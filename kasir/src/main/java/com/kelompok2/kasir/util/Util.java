package com.kelompok2.kasir.util;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Build;
import android.support.v4.app.FragmentManager;
import android.view.Window;

import com.kelompok2.kasir.ui.MejaDialog;
import com.kelompok2.kasir.ui.TopupDialog;

/**
 * Created by Chinta Kumala on 16/04/2016.
 */
public class Util {

    public static MejaDialog showMejaDialog(FragmentManager fragmentManager) {
        MejaDialog dialog = MejaDialog.newIntance();
        dialog.show(fragmentManager, "dialogMessage");
        return dialog;
    }

    public static TopupDialog showTopupDialog(FragmentManager fragmentManager) {
        TopupDialog dialog = TopupDialog.newIntance();
        dialog.show(fragmentManager, "dialogMessage");
        return dialog;
    }


    public static ProgressDialog createProgressDialog(Context context, String message) {
        ProgressDialog dialog = new ProgressDialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        }
        dialog.setMessage(message);
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
        return dialog;
    }

}
