package com.kelompok2.kasir.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;

import com.kelompok2.kasir.R;

/**
 * Created by Chinta Kumala on 14/04/2016.
 */
public class TopupDialog extends DialogFragment {
    private OnMessageClosed messageClosed;

    public static TopupDialog newIntance() {
        TopupDialog mejaDialog = new TopupDialog();
        return mejaDialog;
    }

    public void setOnMessageClosed(OnMessageClosed messageClosed) {
        this.messageClosed = messageClosed;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_topup, container, false);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NO_TITLE, R.style.CustomDialog);
    }

    @Override
    public void onViewCreated(View v, @Nullable Bundle savedInstanceState) {
        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
        setStyle(STYLE_NO_FRAME, 0);
        final EditText txtId = (EditText) v.findViewById(R.id.dialog_id_customer);
        final EditText txtNominal = (EditText) v.findViewById(R.id.dialog_nominal);
        Button btn = (Button) v.findViewById(R.id.dialog_btn_topup);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (messageClosed != null) {
                    messageClosed.onClosed(txtId.getText().toString(), txtNominal.getText().toString());
                }
                dismiss();
            }
        });
    }

    public interface OnMessageClosed {
        public void onClosed(String idUser, String nominal);
    }
}
