package com.kelompok2.kasir.ui;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.kelompok2.kasir.R;
import com.kelompok2.kasir.data.Order;
import com.kelompok2.kasir.data.Transaksi;
import com.kelompok2.kasir.koneksi.ClientServiceActivity;
import com.kelompok2.kasir.koneksi.Filter;
import com.restaurant.connection.ServiceResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Chandra on 4/17/2016.
 */
public class ConfirmationActivity extends NavigationActivity {

    TextView txtIdUser;
    TextView txtIdOrder;
    TextView txtTotal;
    Button btnProses;
    RadioButton radioCash;
    RadioButton radioEmoney;

    String idMeja;
    BroadcastReceiver receiver = getReceiver();

    RecyclerView recyclerView;
    GridLayoutManager manager;
    PesananAdapter adapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirmation_order);
        initToolbar();
        txtIdOrder = (TextView) findViewById(R.id.txtIdOrder);
        txtIdUser = (TextView) findViewById(R.id.txtIdUser);
        txtTotal = (TextView) findViewById(R.id.txtTotal);
        btnProses = (Button) findViewById(R.id.btnProses);
        radioCash = (RadioButton) findViewById(R.id.radioCash);
        radioEmoney = (RadioButton) findViewById(R.id.radioEmoney);
        radioCash.setChecked(true);
        radioCash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radioCash.setChecked(true);
                radioEmoney.setChecked(false);
            }
        });

        radioEmoney.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radioEmoney.setChecked(true);
                radioCash.setChecked(false);
            }
        });

        btnProses.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Transaksi transaksiData = new Transaksi();
                transaksiData.setIdMeja(idMeja);
                transaksiData.setIdOrder(txtIdOrder.getText().toString());
                transaksiData.setIdUser(txtIdUser.getText().toString());
                if (radioCash.isChecked()) {
                    transaksiData.setType("cash");
                } else {
                    transaksiData.setType("e-money");
                }

                transaksi(transaksiData.toString());
            }
        });

        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        manager = new GridLayoutManager(ConfirmationActivity.this, 1);
        recyclerView.setLayoutManager(manager);
        recyclerView.setAdapter(adapter = new PesananAdapter(ConfirmationActivity.this));
        manager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                return position == 0 ? manager.getSpanCount() : 1;

            }
        });
        recyclerView.setHasFixedSize(false);

        parsingJsonCariOrder(getIntent().getStringExtra("data"));
    }

    @Override
    public void initToolbar() {
        super.initToolbar();
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setNavigationBack(this, "Konfirmasi Pembayaran");
    }

    void parsingJsonCariOrder(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            txtIdUser.setText(jsonObject.getString("id_user"));
            txtIdOrder.setText(jsonObject.getString("id_order"));
            txtTotal.setText(jsonObject.getString("total_harga"));
            idMeja = jsonObject.getString("id_meja");
            JSONArray jsonArray = jsonObject.getJSONArray("daftar_order");
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject json = jsonArray.getJSONObject(i);
                Order order = new Order();
                order.setIdMenu(json.getString("id_menu"));
                order.setJumlah(json.getString("jumlah"));
                order.setStatus(json.getString("status"));
                adapter.addOrder(order);
            }
        } catch (JSONException e) {
            Log.e("Confirm", "" + e);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(receiver, new IntentFilter(Filter.TRANSAKSI));
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(receiver);
    }

    @Override
    public void successResponse(ServiceResponse response, String filter) {
        super.successResponse(response, filter);
        Toast.makeText(this, "Transaksi Berhasil", Toast.LENGTH_LONG).show();
        finish();
    }

    @Override
    public void badResponse(ServiceResponse response, String filter) {
        super.badResponse(response, filter);
        Toast.makeText(this, response.getContent(), Toast.LENGTH_LONG).show();
    }

    class PesananAdapter extends RecyclerView.Adapter {

        private ArrayList<Order> orders;
        private Context context;

        public PesananAdapter(Context context) {
            this.context = context;
            orders = new ArrayList<>();
        }

        public void addOrder(Order order) {
            orders.add(order);
            notifyDataSetChanged();
        }

        public Order getItem(int position) {
            return orders.get(position);
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_order,
                    parent, false);
            return new Holder(v);
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            ((Holder) holder).setValue(orders.get(position));
        }

        @Override
        public int getItemCount() {
            return orders.size();
        }
    }

    private class Holder extends RecyclerView.ViewHolder {
        TextView txtNama;
        TextView txtJumlah;

        public Holder(View itemView) {
            super(itemView);
            txtNama = (TextView) itemView.findViewById(R.id.item_id_menu);
            txtJumlah = (TextView) itemView.findViewById(R.id.item_jumlah);
        }

        public void setValue(Order order) {
            txtNama.setText(order.getIdMenu());
            txtJumlah.setText(order.getJumlah() + " - " + order.getStatus());
        }
    }
}
