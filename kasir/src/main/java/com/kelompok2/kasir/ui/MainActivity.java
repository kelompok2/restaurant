package com.kelompok2.kasir.ui;

import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.kelompok2.kasir.R;
import com.kelompok2.kasir.koneksi.Filter;
import com.kelompok2.kasir.util.Util;
import com.restaurant.connection.ServiceResponse;

/**
 * Created by Chinta Kumala on 16/04/2016.
 */
public class MainActivity extends NavigationActivity {

    LinearLayout btnQr;
    LinearLayout btnHistory;
    LinearLayout btnExit;
    LinearLayout btnTopup;

    BroadcastReceiver receiver = getReceiver();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btnQr = (LinearLayout) findViewById(R.id.menu_kode_qr);
        btnQr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent intent = new Intent(MainActivity.this, OrderActivity.class);
//                startActivity(intent);
                Util.showMejaDialog(getSupportFragmentManager()).setOnMessageClosed(new MejaDialog.OnMessageClosed() {
                    @Override
                    public void onClosed(String noMeja) {
                        String parameter = "no_meja=" + noMeja;
                        showProgressMessage(MainActivity.this, "Harap tunggu");
                        getOrder(parameter);
                    }
                });
            }
        });

        btnTopup = (LinearLayout) findViewById(R.id.menu_topup);
        btnTopup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Util.showTopupDialog(getSupportFragmentManager()).setOnMessageClosed(new TopupDialog.OnMessageClosed() {
                    @Override
                    public void onClosed(String idUser, String nominal) {

                        String parameter = "id_user=" + idUser + "&nominal=" + nominal;
                        parameter = parameter.replaceAll("%20", "+");
                        parameter = parameter.replaceAll("%3D", "=");
                        showProgressMessage(MainActivity.this, "Harap tunggu");
                        topup(parameter);
                    }
                });
            }


        });

        btnHistory = (LinearLayout) findViewById(R.id.menu_history);
        btnHistory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


            }
        });
        btnExit = (LinearLayout) findViewById(R.id.menu_exit);
        btnExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
            }
        });

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setNavigationTitle("Halaman Utama");
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(receiver, new IntentFilter(Filter.GET_ORDER));
        registerReceiver(receiver, new IntentFilter(Filter.TOP_UP));
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(receiver);
    }

    @Override
    public void successResponse(ServiceResponse response, String filter) {
        super.successResponse(response, filter);
        dismissProgressMessage();
        if (filter.equals(Filter.TOP_UP)) {
            Toast.makeText(this, response.getContent(), Toast.LENGTH_LONG).show();
        } else {
            Intent intent = new Intent(MainActivity.this, ConfirmationActivity.class);
            intent.putExtra("data", response.getContent());
            startActivity(intent);
        }

    }

    @Override
    public void badResponse(ServiceResponse response, String filter) {
        super.badResponse(response, filter);
        dismissProgressMessage();
        Toast.makeText(this, response.getContent(), Toast.LENGTH_LONG).show();
    }
}
