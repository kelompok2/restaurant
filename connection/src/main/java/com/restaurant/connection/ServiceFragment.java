package com.restaurant.connection;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;

/**
 * Created by User on 2/5/2016.
 */
public class ServiceFragment extends Fragment {

    public void sendGetRequest(String uri, String filter){
        ServiceRequest serviceRequest = new ServiceRequest();
        serviceRequest.setUri(uri);
        serviceRequest.setMethod(Service.METHOD_GET);
        serviceRequest.setFilter(filter);

        Intent i = new Intent(getActivity(), Service.class);
        i.putExtra(Service.TAG_REQUEST, serviceRequest);
        getActivity().startService(i);
    }

    public void sendPostRequest(String uri, String parameter, String filter, String contenType){
        ServiceRequest serviceRequest = new ServiceRequest();
        serviceRequest.setUri(uri);
        serviceRequest.setMethod(Service.METHOD_POST);
        serviceRequest.setContent(parameter);
        serviceRequest.setFilter(filter);
        serviceRequest.setContentType(contenType);

        Intent i = new Intent(getActivity(), Service.class);
        i.putExtra(Service.TAG_REQUEST, serviceRequest);
        getActivity().startService(i);
    }

    public void sendDeleteRequest(String uri, String parameter, String filter, String contenType){
        ServiceRequest serviceRequest = new ServiceRequest();
        serviceRequest.setUri(uri);
        serviceRequest.setMethod(Service.METHOD_DELETE);
        serviceRequest.setContent(parameter);
        serviceRequest.setFilter(filter);
        serviceRequest.setContentType(contenType);

        Intent i = new Intent(getActivity(), Service.class);
        i.putExtra(Service.TAG_REQUEST, serviceRequest);
        getActivity().startService(i);
    }

    public void sendPutRequest(String uri, String parameter, String filter, String contenType){
        ServiceRequest serviceRequest = new ServiceRequest();
        serviceRequest.setUri(uri);
        serviceRequest.setMethod(Service.METHOD_PUT);
        serviceRequest.setContent(parameter);
        serviceRequest.setFilter(filter);
        serviceRequest.setContentType(contenType);

        Intent i = new Intent(getActivity(), Service.class);
        i.putExtra(Service.TAG_REQUEST, serviceRequest);
        getActivity().startService(i);
    }

    public BroadcastReceiver getReceiver() {
        return new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                ServiceResponse serviceResponse = (ServiceResponse) intent.getSerializableExtra(Service.TAG_RESPONSE);

                switch (serviceResponse.getCode()) {
                    case 200:
                    case 204:
                        successResponse(serviceResponse, intent.getAction());
                        break;
                    case 400:
                    case 401:
                    case 404:
                        badResponse(serviceResponse, intent.getAction());
                        break;
                    default:
                        badResponse(serviceResponse, intent.getAction());
                        break;
                }
            }
        };
    }

    public void successResponse(ServiceResponse response, String filter){

    }

    public void badResponse(ServiceResponse response, String filter) {

    }
}
