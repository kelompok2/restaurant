package com.restaurant.connection;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import java.util.HashMap;

/**
 * Created by User on 2/5/2016.
 */
public class ServiceActivity extends AppCompatActivity {

    public static HashMap responseMap = new HashMap();

    public void sendGetRequest(String uri, String filter){
        ServiceRequest serviceRequest = new ServiceRequest();
        serviceRequest.setUri(uri);
        serviceRequest.setMethod(Service.METHOD_GET);
        serviceRequest.setFilter(filter);

        Intent i = new Intent(this, Service.class);
        i.putExtra(Service.TAG_REQUEST, serviceRequest);
        startService(i);
    }

    public void sendPostRequest(String uri, String parameter, String filter, String contenType){
        ServiceRequest serviceRequest = new ServiceRequest();
        serviceRequest.setUri(uri);
        serviceRequest.setMethod(Service.METHOD_POST);
        serviceRequest.setContent(parameter);
        serviceRequest.setFilter(filter);
        serviceRequest.setContentType(contenType);

        Intent i = new Intent(this, Service.class);
        i.putExtra(Service.TAG_REQUEST, serviceRequest);
        startService(i);
    }

    public void sendDeleteRequest(String uri, String parameter, String filter, String contenType){
        ServiceRequest serviceRequest = new ServiceRequest();
        serviceRequest.setUri(uri);
        serviceRequest.setMethod(Service.METHOD_DELETE);
        serviceRequest.setContent(parameter);
        serviceRequest.setFilter(filter);
        serviceRequest.setContentType(contenType);

        Intent i = new Intent(this, Service.class);
        i.putExtra(Service.TAG_REQUEST, serviceRequest);
        startService(i);
    }

    public void sendPutRequest(String uri, String parameter, String filter, String contenType){
        ServiceRequest serviceRequest = new ServiceRequest();
        serviceRequest.setUri(uri);
        serviceRequest.setMethod(Service.METHOD_PUT);
        serviceRequest.setContent(parameter);
        serviceRequest.setFilter(filter);
        serviceRequest.setContentType(contenType);

        Intent i = new Intent(this, Service.class);
        i.putExtra(Service.TAG_REQUEST, serviceRequest);
        startService(i);
    }
    public BroadcastReceiver getReceiver() {
        return new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                ServiceResponse serviceResponse = (ServiceResponse) intent.getSerializableExtra(Service.TAG_RESPONSE);
                Log.d("Intent Filter", intent.getAction());
                if (serviceResponse == null) {
                    Toast.makeText(ServiceActivity.this, "Koneksi bermasalah.\nPeriksa jaringan anda dan silahkan coba lagi.", Toast.LENGTH_LONG).show();
                    return;
                }
                switch (serviceResponse.getCode()) {
                    case 200:
                    case 204:
                        successResponse(serviceResponse, intent.getAction());
                        break;
                    case 400:
                    case 401:
                    case 404:
                        badResponse(serviceResponse, intent.getAction());
                        break;
                    default:
                        badResponse(serviceResponse, intent.getAction());
                        break;
                }
            }
        };
    }


    public void successResponse(ServiceResponse response, String filter){
        responseMap.put(filter, response);
    }

    public void pendingSuccessResponse(ServiceResponse response, String filter){
        Log.d("Contain", ""+responseMap.containsKey(filter));
        if (responseMap.containsKey(filter)) {
            response = (ServiceResponse) responseMap.get(filter);
        }

    }

    public void getPendingResponse(String filter){
        ServiceResponse response = (ServiceResponse) responseMap.get(filter);
        pendingSuccessResponse(response, filter);
    }

    public void badResponse(ServiceResponse response, String filter) {

    }
}
