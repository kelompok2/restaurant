package com.restaurant.connection;

import android.app.IntentService;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;

/**
 * Created by User on 2/5/2016.
 */
public class Service extends IntentService {

    public static final String TAG_RESPONSE = "serviceResponse";
    public static final String TAG_REQUEST = "serviceRequest";

    public static final String METHOD_GET = "GET";
    public static final String METHOD_POST = "POST";
    public static final String METHOD_PUT = "PUT";
    public static final String METHOD_DELETE = "DELETE";

    public static final String CONTENT_TYPE_BLANK = "";
    public static final String CONTENT_TYPE_URLENCODED = "application/x-www-form-urlencoded";
    public static final String CONTENT_TYPE_JSON = "application/json";

    String uri = "";
    String method = "";
    String filter = "";
    String parameter = "";
    String contentType = "";

    int response_code;
    String response_message;
    String response_content;

    public Service() {
        super("Service");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        URLConnection urlConnection = new URLConnection();

        ServiceRequest serviceRequest = (ServiceRequest) intent.getSerializableExtra(TAG_REQUEST);
        uri = serviceRequest.getUri();
        method = serviceRequest.getMethod();
        filter = serviceRequest.getFilter();
        contentType = serviceRequest.getContentType();
        switch (method) {
            case METHOD_GET:
                urlConnection.Get(uri);
                break;
            case METHOD_POST:
                parameter = serviceRequest.getContent();
                urlConnection.Post(uri, parameter, contentType);
                break;
            case METHOD_PUT:
                parameter = serviceRequest.getContent();
                urlConnection.Put(uri, parameter, contentType);
                break;
            case METHOD_DELETE:
                parameter = serviceRequest.getContent();
                urlConnection.Delete(uri, parameter, contentType);
                break;
        }

        Log.i("", "********************* Service Request **********************");
        Log.i("URI", uri);
        Log.i("Filter", filter);
        Log.i("Method", method);
        Log.i("Content-type", "" + contentType);
        Log.i("Parameter", parameter);
        Log.i("", "************************************************************");

        ServiceResponse serviceResponse = new ServiceResponse();
        serviceResponse.setUri(uri);
        serviceResponse.setMethod(method);
        serviceResponse.setCode(response_code);
        serviceResponse.setMessage(response_message);
        serviceResponse.setContent(response_content);

        Intent broadcastIntent = new Intent();
        broadcastIntent.setAction(filter);
        broadcastIntent.putExtra(TAG_RESPONSE, serviceResponse);
        LocalBroadcastManager localBroadcastManager = LocalBroadcastManager.getInstance(this);
        localBroadcastManager.sendBroadcast(broadcastIntent);
        sendBroadcast(broadcastIntent);
    }

    public class URLConnection {

        public void Get(String uri) {
            BufferedReader reader = null;
            try {
                java.net.URL url = new java.net.URL(uri);
                HttpURLConnection con = (HttpURLConnection) url.openConnection();
                con.setReadTimeout(10000);
                con.setConnectTimeout(15000);
                con.setDoInput(true);
                con.connect();
                InputStream is = con.getInputStream();
                reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
                String data = null;
                response_code = con.getResponseCode();
                response_message = con.getResponseMessage();
                response_content = "";
                while ((data = reader.readLine()) != null) {
                    response_content += data + "\n";
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (reader != null) {
                    try {
                        reader.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }

        public void Post(String uri, String param, String contentType) {
            try {
                java.net.URL url = new java.net.URL(uri);
                HttpURLConnection con = (HttpURLConnection) url.openConnection();
                con.setReadTimeout(10000);
                con.setConnectTimeout(15000);
                con.setDoInput(true);
                con.setDoOutput(true);

                con.setRequestMethod("POST");
                con.setFixedLengthStreamingMode(param.getBytes().length);
                con.setRequestProperty("Content-Type", contentType);

                PrintWriter out = new PrintWriter(con.getOutputStream());
                out.print(param);
                out.close();

                con.connect();

                response_code = con.getResponseCode();
                response_message = con.getResponseMessage();
                if (response_code == 200 || response_code == 204) {
                    BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream()));
                    StringBuilder sb = new StringBuilder();
                    String line;
                    while ((line = br.readLine()) != null) {

                        sb.append(line + "\n");
                    }
                    br.close();
                    response_content = sb.toString();
                } else {
                    BufferedReader br = new BufferedReader(new InputStreamReader(con.getErrorStream()));
                    StringBuilder sb = new StringBuilder();
                    String line;
                    while ((line = br.readLine()) != null) {
                        sb.append(line + "\n");
                    }
                    br.close();
                    response_content = sb.toString();
                }


                con.disconnect();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (ProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        public void Put(String uri, String param, String contentType) {
            try {
                java.net.URL url = new java.net.URL(uri);
                HttpURLConnection con = (HttpURLConnection) url.openConnection();
                con.setDoInput(true);
                con.setDoOutput(true);
                con.setRequestMethod("PUT");
                con.setFixedLengthStreamingMode(param.getBytes().length);
                con.setRequestProperty("Content-Type", contentType);

                PrintWriter out = new PrintWriter(con.getOutputStream());
                out.print(param);
                out.close();

                con.connect();

                response_code = con.getResponseCode();
                response_message = con.getResponseMessage();
                if (response_code == 200 || response_code == 204) {
                    BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream()));
                    StringBuilder sb = new StringBuilder();
                    String line;
                    while ((line = br.readLine()) != null) {

                        sb.append(line + "\n");
                    }
                    br.close();
                    response_content = sb.toString();
                } else {
                    BufferedReader br = new BufferedReader(new InputStreamReader(con.getErrorStream()));
                    StringBuilder sb = new StringBuilder();
                    String line;
                    while ((line = br.readLine()) != null) {

                        sb.append(line + "\n");
                    }
                    br.close();
                    response_content = sb.toString();
                }
                con.disconnect();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (ProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        public void Delete(String uri, String id, String contentType) {
            try {
                java.net.URL url = new java.net.URL(uri + id);
                HttpURLConnection con = (HttpURLConnection) url.openConnection();
                con.setDoInput(true);
                con.setDoOutput(true);

                con.setRequestMethod("DELETE");
                con.setFixedLengthStreamingMode(id.getBytes().length);
                con.setRequestProperty("Content-Type", contentType);

                PrintWriter out = new PrintWriter(con.getOutputStream());
                out.print(id);
                out.close();

                con.connect();

                response_code = con.getResponseCode();
                response_message = con.getResponseMessage();
                if (response_code == 200 || response_code == 204) {
                    BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream()));
                    StringBuilder sb = new StringBuilder();
                    String line;
                    while ((line = br.readLine()) != null) {

                        sb.append(line + "\n");
                    }
                    br.close();
                    response_content = sb.toString();
                } else {
                    BufferedReader br = new BufferedReader(new InputStreamReader(con.getErrorStream()));
                    StringBuilder sb = new StringBuilder();
                    String line;
                    while ((line = br.readLine()) != null) {

                        sb.append(line + "\n");
                    }
                    br.close();
                    response_content = sb.toString();
                }
                con.disconnect();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (ProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }
}
