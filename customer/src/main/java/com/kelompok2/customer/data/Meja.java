package com.kelompok2.customer.data;

import java.io.Serializable;

/**
 * Created by Chinta Kumala on 12/04/2016.
 */
public class Meja implements Serializable {
    private String idMeja;
    private String noMeja;
    private String status;

    public String getIdMeja() {
        return idMeja;
    }

    public void setIdMeja(String idMeja) {
        this.idMeja = idMeja;
    }

    public String getNoMeja() {
        return noMeja;
    }

    public void setNoMeja(String noMeja) {
        this.noMeja = noMeja;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
