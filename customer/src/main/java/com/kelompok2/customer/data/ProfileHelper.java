package com.kelompok2.customer.data;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Chinta Kumala on 04/04/2016.
 */
public class ProfileHelper {
    public static final String ID_USER = "id_user";
    public static final String EMAIL = "email";
    public static final String NAMA_USER = "nama";
    public static final String TEMPAT_LAHIR = "tempat_lahir";
    public static final String TANGGAL_LAHIR = "tanggla_lahir";
    public static final String FOTO_URL = "foto";
    public static final String SALDO = "SALDO";
    public static final String ID_ORDER = "idOrder";

    private SharedPreferences prefs;
    Context context;
    private static ProfileHelper helper;

    public static ProfileHelper getInstance(Context context) {
        if (helper == null) {
            helper = new ProfileHelper(context);
        }
        return helper;
    }

    private ProfileHelper(Context context) {
        this.context = context;
        prefs = context.getSharedPreferences("ProfileHelper", 0);
    }

    public void setIdUser(String idUser) {
        if (prefs != null) {
            SharedPreferences.Editor editor = prefs.edit();
            editor.putString(ID_USER, idUser);
            editor.apply();
        }
    }

    public String getIdUser() {
        if (prefs != null) {
            return prefs.getString(ID_USER, null);
        }
        return null;
    }

    public void setEmail(String email) {
        if (prefs != null) {
            SharedPreferences.Editor editor = prefs.edit();
            editor.putString(EMAIL, email);
            editor.apply();
        }
    }

    public String getEmail() {
        if (prefs != null) {
            return prefs.getString(EMAIL, null);
        }
        return null;
    }

    public void setNamaUser(String namaUser) {
        if (prefs != null) {
            SharedPreferences.Editor editor = prefs.edit();
            editor.putString(NAMA_USER, namaUser);
            editor.apply();
        }
    }

    public String getNamaUser() {
        if (prefs != null) {
            return prefs.getString(NAMA_USER, null);
        }
        return null;
    }

    public void setTempatLahir(String tempatLahir) {
        if (prefs != null) {
            SharedPreferences.Editor editor = prefs.edit();
            editor.putString(TEMPAT_LAHIR, tempatLahir);
            editor.apply();
        }
    }

    public String getTempatLahir() {
        if (prefs != null) {
            return prefs.getString(TEMPAT_LAHIR, null);
        }
        return null;
    }

    public void setTanggalLahir(String tanggalLahir) {
        if (prefs != null) {
            SharedPreferences.Editor editor = prefs.edit();
            editor.putString(TANGGAL_LAHIR, tanggalLahir);
            editor.apply();
        }
    }

    public String getTanggalLahir() {
        if (prefs != null) {
            return prefs.getString(TANGGAL_LAHIR, null);
        }
        return null;
    }

    public void setSaldo(String saldo) {
        if (prefs != null) {
            SharedPreferences.Editor editor = prefs.edit();
            editor.putString(SALDO, saldo);
            editor.apply();
        }
    }

    public String getSaldo() {
        if (prefs != null) {
            return prefs.getString(SALDO, null);
        }
        return null;
    }

    public void setFotoUrl(String fotoUrl) {
        if (prefs != null) {
            SharedPreferences.Editor editor = prefs.edit();
            editor.putString(FOTO_URL, fotoUrl);
            editor.apply();
        }
    }

    public String getFotoUrl() {
        if (prefs != null) {
            return prefs.getString(FOTO_URL, null);
        }
        return null;
    }

    public String getIdOrder() {
        if (prefs != null) {
            return prefs.getString(ID_ORDER, null);
        }
        return null;
    }

    public void setIdOrder(String idOrder) {
        if (prefs != null) {
            SharedPreferences.Editor editor = prefs.edit();
            editor.putString(ID_ORDER, idOrder);
            editor.apply();
        }
    }
}
