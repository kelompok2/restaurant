package com.kelompok2.customer.data;

import android.net.Uri;

/**
 * Created by Chinta Kumala on 28/03/2016.
 */
public class Login {
    public String email;
    public String password;

    @Override
    public String toString() {
        String result;

        result = Uri.encode("email=" + email +
                        "&password=" + password);
        result=result.replaceAll("%20","+");
        result=result.replaceAll("%3D","=");
        result=result.replaceAll("%26","&");
        return result;
    }
}
