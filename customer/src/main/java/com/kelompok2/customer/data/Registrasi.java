package com.kelompok2.customer.data;

import android.net.Uri;

/**
 * Created by Chinta Kumala on 29/03/2016.
 */
public class Registrasi {

    public String email;
    public String password;
    public String konfPassword;
    public String noKtp;
    public String nama;

    @Override
    public String toString() {
        String result;

        result = Uri.encode("id_user=" + noKtp +
                "&password=" + password +
                "&email=" + email +
                "&nama=" + nama);
        result=result.replaceAll("%20", "+");
        result=result.replaceAll("%3D","=");
        result=result.replaceAll("%26","&");
        return result;
    }
}
