package com.kelompok2.customer.data;

import android.net.Uri;

import java.io.Serializable;

/**
 * Created by Chinta Kumala on 15/04/2016.
 */
public class PilihMeja implements Serializable {
    public String id_user;
    public String id_meja;

    @Override
    public String toString() {
        String result;
        result = Uri.encode("id_user=" + id_user +
                "&id_meja=" + id_meja);
        result=result.replaceAll("%20","+");
        result=result.replaceAll("%3D","=");
        result=result.replaceAll("%26","&");
        return result;
    }
}
