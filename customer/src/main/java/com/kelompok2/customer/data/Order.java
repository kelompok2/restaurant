package com.kelompok2.customer.data;

import android.net.Uri;

/**
 * Created by Chinta Kumala on 15/04/2016.
 */
public class Order {
    public String idOrder;
    public String idUser;
    public String idMenu;
    public String jumlah;

    public String getIdOrder() {
        return idOrder;
    }

    public void setIdOrder(String idOrder) {
        this.idOrder = idOrder;
    }

    public String getIdUser() {
        return idUser;
    }

    public void setIdUser(String idUser) {
        this.idUser = idUser;
    }

    public String getIdMenu() {
        return idMenu;
    }

    public void setIdMenu(String idMenu) {
        this.idMenu = idMenu;
    }

    public String getJumlah() {
        return jumlah;
    }

    public void setJumlah(String jumlah) {
        this.jumlah = jumlah;
    }

    @Override
    public String toString() {
        String result;

        result = Uri.encode("id_order=" + idOrder +
                "&id_user=" + idUser +
                "&jumlah=" + jumlah +
                "&id_menu=" + idMenu);
        result=result.replaceAll("%20","+");
        result=result.replaceAll("%3D","=");
        result=result.replaceAll("%26","&");
        return result;
    }
}
