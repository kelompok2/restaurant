package com.kelompok2.customer.ui.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.kelompok2.customer.R;
import com.kelompok2.customer.data.Order;
import com.kelompok2.customer.data.ProfileHelper;
import com.kelompok2.customer.koneksi.Filter;
import com.kelompok2.customer.ui.dialog.OrderDialog;
import com.kelompok2.customer.util.Util;
import com.restaurant.connection.ServiceResponse;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Chinta Kumala on 15/04/2016.
 */
public class DaftarPesananActivity extends NavigationActivity {

    FloatingActionButton fab;
    FloatingActionButton fabFinish;
    RecyclerView recyclerView;
    GridLayoutManager manager;
    PesananAdapter adapter;
    ProfileHelper profileHelper;
    Order order;
    BroadcastReceiver receiver = getReceiver();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_daftar_pesanan);
        order = new Order();
        profileHelper = ProfileHelper.getInstance(this);
        initToolbar();
        initComponents();
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(receiver, new IntentFilter(Filter.CUSTOMER_PESAN_MENU));
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(receiver);
    }

    @Override
    public void initToolbar() {
        super.initToolbar();
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setNavigationBack(this, "Daftar Pesanan");
    }

    private void initComponents() {
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        fab = (FloatingActionButton) findViewById(R.id.daftar_pesanan_fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DaftarPesananActivity.this, ScanQRActivity.class);
                startActivityForResult(intent, 1);
            }
        });

        fabFinish = (FloatingActionButton) findViewById(R.id.daftar_pesanan_fab_ok);
        fabFinish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(DaftarPesananActivity.this, "Pesanan berhasil dikirim", Toast.LENGTH_LONG).show();
                finish();
            }
        });
        manager = new GridLayoutManager(DaftarPesananActivity.this, 1);
        recyclerView.setLayoutManager(manager);
        recyclerView.setAdapter(adapter = new PesananAdapter(DaftarPesananActivity.this));
        manager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                return position == 0 ? manager.getSpanCount() : 1;

            }
        });
        recyclerView.setHasFixedSize(false);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            String idMenu = data.getStringExtra("idMenu");
            order = new Order();
            order.idMenu = idMenu;
            order.idOrder = profileHelper.getIdOrder();
            order.idUser = profileHelper.getIdUser();

            Log.d("Hasil Scan QR", idMenu);
            Util.showOrderDialog(getSupportFragmentManager()).setOnMessageClosed(new OrderDialog.OnMessageClosed() {
                @Override
                public void onClosed() {

                }

                @Override
                public void onProcess(String jumlah) {
                    order.jumlah = jumlah;
                    showProgressMessage(DaftarPesananActivity.this, "Sedang mengirim pesanan");
                    pesanMenu(order.toString());
                }
            });
        }
    }

    @Override
    public void successResponse(ServiceResponse response, String filter) {
        super.successResponse(response, filter);
        dismissProgressMessage();
        if (filter.equals(Filter.CUSTOMER_PESAN_MENU)) {
            try {
                JSONObject jsonObject = new JSONObject(response.getContent());
                Toast.makeText(this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                adapter.addOrder(order);
            } catch (JSONException e) {
                Log.e("DaftarPesananActivity", "" + e);
            }
        }
    }

    @Override
    public void badResponse(ServiceResponse response, String filter) {
        super.badResponse(response, filter);
        dismissProgressMessage();
        try {
            JSONObject jsonObject = new JSONObject(response.getContent());
            Toast.makeText(this, jsonObject.getString("message"), Toast.LENGTH_LONG).show();
        } catch (JSONException e) {
            Log.e("DaftarPesananActivity", "" + e);
        }
    }

    class PesananAdapter extends RecyclerView.Adapter {

        private ArrayList<Order> orders;
        private Context context;

        public PesananAdapter(Context context) {
            this.context = context;
            orders = new ArrayList<>();
        }

        public void addOrder(Order order) {
            orders.add(order);
            notifyDataSetChanged();
        }

        public Order getItem(int position) {
            return orders.get(position);
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_daftar_pesanan,
                    parent, false);
            return new Holder(v);
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            ((Holder) holder).setValue(orders.get(position));
        }

        @Override
        public int getItemCount() {
            return orders.size();
        }
    }

    private class Holder extends RecyclerView.ViewHolder {
        TextView txtNama;
        TextView txtJumlah;
        ImageView img;

        public Holder(View itemView) {
            super(itemView);
            txtNama = (TextView) itemView.findViewById(R.id.item_order_nama_menu);
            txtJumlah = (TextView) itemView.findViewById(R.id.item_order_jumlah);
            img = (ImageView) itemView.findViewById(R.id.item_order_delete);
        }

        public void setValue(Order order) {
            txtNama.setText(order.getIdMenu());
            txtJumlah.setText(order.getJumlah());
        }
    }

}
