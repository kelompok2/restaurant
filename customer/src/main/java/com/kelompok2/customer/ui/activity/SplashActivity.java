package com.kelompok2.customer.ui.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.kelompok2.customer.R;
import com.kelompok2.customer.data.ProfileHelper;

/**
 * Created by Chinta Kumala on 29/03/2016.
 */
public class SplashActivity extends Activity {
    private Button btn_login;
    private Button btn_daftar;
    ProfileHelper profileHelper;
    ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        imageView = (ImageView) findViewById(R.id.img_backgroundSplash);
        Glide.with(this).load(R.drawable.bg_pict).crossFade().into(imageView);

        btn_daftar = (Button) findViewById(R.id.btn_daftarbaru);
        btn_daftar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(SplashActivity.this, RegistrasiActivity.class);
                startActivity(i);
            }
        });
        btn_login = (Button) findViewById(R.id.btn_login1);
        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(SplashActivity.this, LoginActivity.class);
                startActivity(i);
            }
        });


        profileHelper = ProfileHelper.getInstance(this);
        if (profileHelper.getIdUser() != null) {
            Intent i = new Intent(SplashActivity.this, MainActivity.class);
            startActivity(i);
            finish();
        }
    }
}
