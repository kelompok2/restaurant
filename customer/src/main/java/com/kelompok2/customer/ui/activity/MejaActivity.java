package com.kelompok2.customer.ui.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.kelompok2.customer.R;
import com.kelompok2.customer.data.Meja;
import com.kelompok2.customer.data.PilihMeja;
import com.kelompok2.customer.data.ProfileHelper;
import com.kelompok2.customer.koneksi.Filter;
import com.kelompok2.customer.ui.dialog.MejaDialog;
import com.kelompok2.customer.util.Util;
import com.restaurant.connection.ServiceResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Chinta Kumala on 12/04/2016.
 */
public class MejaActivity extends NavigationActivity {

    RecyclerView recyclerView;
    GridLayoutManager manager;
    MejaAdapter adapter;
    BroadcastReceiver receiver = getReceiver();

    ProfileHelper profileHelper;
    PilihMeja pilihMeja;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_meja);
        profileHelper = ProfileHelper.getInstance(this);
        initToolbar();
        initComponents();
        getMeja();
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(receiver, new IntentFilter(Filter.CUSTOMER_MEJA));
        registerReceiver(receiver, new IntentFilter(Filter.CUSTOMER_PILIH_MEJA));
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(receiver);
    }

    @Override
    public void successResponse(ServiceResponse response, String filter) {
        super.successResponse(response, filter);
        dismissProgressMessage();
        switch (filter) {
            case Filter.CUSTOMER_MEJA:
                try {
                    Log.d("Jumlah", response.getContent());
                    JSONObject json = new JSONObject(response.getContent());
                    JSONArray jsonArray = json.getJSONArray("meja");
                    for (int i = 0; i < jsonArray.length(); i++) {

                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        Meja meja = new Meja();
                        meja.setIdMeja(jsonObject.getString("id_meja"));
                        meja.setNoMeja(jsonObject.getString("no_meja"));
                        meja.setStatus(jsonObject.getString("status"));
                        adapter.addMeja(meja);

                    }
                } catch (JSONException e) {
                    Log.e("MejaActivity", "" + e);
                }
                break;
            case Filter.CUSTOMER_PILIH_MEJA:
                try {
                    JSONObject jsonObject = new JSONObject(response.getContent());
                    String idOrder = jsonObject.getString("id_order");
                    profileHelper.setIdOrder(idOrder);
                    Intent i = new Intent(MejaActivity.this, DaftarPesananActivity.class);
                    startActivity(i);
                    finish();
                    Log.d("PilihMeja", response.getContent());
                } catch (JSONException e) {
                    Log.e("MejaActivity", "" + e);
                }
                break;
        }
    }

    @Override
    public void badResponse(ServiceResponse response, String filter) {
        super.badResponse(response, filter);
        dismissProgressMessage();
        try {
            JSONObject jsonObject = new JSONObject(response.getContent());
            String message = jsonObject.getString("message");
            Toast.makeText(this, message, Toast.LENGTH_LONG).show();
            if (message.equals("Anda telah memilih tempat duduk.")) {
                finish();
            }
        } catch (JSONException e) {
            Log.e("MejaActivity", "" + e);
        }
        Log.i("BadResponse", response.getContent());
    }

    @Override
    public void initToolbar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setNavigationBack(this, "Daftar Meja");
    }

    private void initComponents() {
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        manager = new GridLayoutManager(MejaActivity.this, 1);
        recyclerView.setLayoutManager(manager);
        recyclerView.setAdapter(adapter = new MejaAdapter(MejaActivity.this));
        manager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                return position == 0 ? manager.getSpanCount() : 1;

            }
        });
        recyclerView.setHasFixedSize(false);
    }

    class MejaAdapter extends RecyclerView.Adapter {

        private ArrayList<Meja> mejas;
        private Context context;

        public MejaAdapter(Context context) {
            this.context = context;
            mejas = new ArrayList<>();
        }

        public void addMeja(Meja meja) {
            mejas.add(meja);
            notifyDataSetChanged();
        }

        public Meja getItem(int position) {
            return mejas.get(position);
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_meja,
                    parent, false);
            return new Holder(v);
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            ((Holder) holder).setValue(mejas.get(position));
        }

        @Override
        public int getItemCount() {
            return mejas.size();
        }
    }

    private class Holder extends RecyclerView.ViewHolder {
        TextView txtNomor;
        TextView txtStatus;

        public Holder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(itemListener);
            txtNomor = (TextView) itemView.findViewById(R.id.nama_pesanan);
            txtStatus = (TextView) itemView.findViewById(R.id.jumlah_pesanan);
        }

        public void setValue(Meja meja) {
            txtNomor.setText(meja.getNoMeja());
            txtStatus.setText(meja.getStatus());
        }
    }

    View.OnClickListener itemListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Meja meja = adapter.getItem(recyclerView.getChildAdapterPosition(v));
            Util.showMejaDialog(getSupportFragmentManager(), meja.getIdMeja()).setOnMessageClosed(new MejaDialog.OnMessageClosed() {
                @Override
                public void onClosed() {

                }

                @Override
                public void onProcess(String idMeja) {
                    pilihMeja = new PilihMeja();
                    pilihMeja.id_meja = idMeja;
                    pilihMeja.id_user = profileHelper.getIdUser();
                    showProgressMessage(MejaActivity.this, "Harap tunggu");
                    pilihMeja(pilihMeja.toString());
                }
            });
        }
    };
}
