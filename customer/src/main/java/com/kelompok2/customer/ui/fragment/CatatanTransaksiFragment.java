package com.kelompok2.customer.ui.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.kelompok2.customer.R;
import com.kelompok2.customer.koneksi.ClientServiceFragment;

/**
 * Created by Chinta Kumala on 02/04/2016.
 */
public class CatatanTransaksiFragment extends ClientServiceFragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_catatan_transaksi, container, false);
        return view;
    }
}
