package com.kelompok2.customer.ui.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.kelompok2.customer.R;
import com.kelompok2.customer.data.Menu;
import com.kelompok2.customer.koneksi.Filter;
import com.restaurant.connection.ServiceResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


/**
 * Created by Chinta Kumala on 13/04/2016.
 */
public class MenuActivity extends NavigationActivity {

    RecyclerView recyclerView;
    GridLayoutManager manager;
    BroadcastReceiver receiver = getReceiver();
    MenuAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        initComponents();
        initToolbar();
        getMenu();
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(receiver, new IntentFilter(Filter.CUSTOMER_MENU));
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(receiver);
    }

    @Override
    public void successResponse(ServiceResponse response, String filter) {
        super.successResponse(response, filter);
        try {
            JSONObject json = new JSONObject(response.getContent());
            JSONArray jsonArray = json.getJSONArray("daftar_menu");
            for (int i = 0; i < jsonArray.length(); i++) {

                JSONObject jsonObject = jsonArray.getJSONObject(i);
                Menu menu = new Menu();
                menu.setNama(jsonObject.getString("id_menu"));
                menu.setIdMenu(jsonObject.getString("nama_menu"));
                menu.setJenis(jsonObject.getString("jenis"));
                menu.setHarga(jsonObject.getString("harga"));
                adapter.addMenu(menu);
            }

        } catch (JSONException e) {
            Log.e("MenuActivity", "" + e);
        }
    }


    @Override
    public void initToolbar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setNavigationBack(this, "Daftar Menu");
    }

    private void initComponents() {
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        manager = new GridLayoutManager(MenuActivity.this, 1);
        recyclerView.setLayoutManager(manager);
        recyclerView.setAdapter(adapter = new MenuAdapter(MenuActivity.this));
        manager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                return position == 0 ? manager.getSpanCount() : 1;
            }
        });
        recyclerView.setHasFixedSize(false);
    }

    class MenuAdapter extends RecyclerView.Adapter {
        private ArrayList<Menu> menus;
        private Context context;

        public MenuAdapter(Context context) {
            this.context = context;
            menus = new ArrayList<>();
        }

        public void addMenu(Menu menu) {
            menus.add(menu);
            notifyDataSetChanged();
        }

        public Menu getItem(int position) {
            return menus.get(position);
        }



        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_menu,
                    parent, false);
            return new Holder(v);
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            ((Holder) holder).setValue(menus.get(position));
        }

        @Override
        public int getItemCount() {
            return menus.size();
        }
    }

        private class Holder extends RecyclerView.ViewHolder {
             TextView txtNama;
            TextView txtJenis;
            TextView txtHarga;


        public Holder(View itemView) {
            super(itemView);
//            itemView.setOnClickListener(itemListener);
            txtNama = (TextView) itemView.findViewById(R.id.text_nama);
            txtJenis = (TextView) itemView.findViewById(R.id.text_jenis);
            txtHarga = (TextView) itemView.findViewById(R.id.text_harga);
        }

        public void setValue(Menu menu) {
            txtNama.setText(menu.getNama());
            txtJenis.setText(menu.getJenis());
            txtHarga.setText(menu.getHarga());
        }

    }
}
