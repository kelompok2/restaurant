package com.kelompok2.customer.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.kelompok2.customer.R;
import com.kelompok2.customer.data.ProfileHelper;
import com.kelompok2.customer.ui.fragment.CatatanTransaksiFragment;
import com.kelompok2.customer.ui.fragment.HalamanUtamaFragment;
import com.kelompok2.customer.ui.fragment.TentangFragment;

/**
 * Created by Chinta Kumala on 01/04/2016.
 */
public class MainActivity extends NavigationActivity {
    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mDrawerToggle;

    ImageView imgHeader;
    TextView txtNameHeader;
    TextView txtSaldo;
    TextView txtLihatProfil;

    ProfileHelper profileHelper;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        profileHelper = ProfileHelper.getInstance(this);
        initToolbar();
        initDrawer();

        selectFragment(new HalamanUtamaFragment());
    }

    @Override
    protected void onResume() {
        super.onResume();
        initHeader();
    }

    @Override
    public void initToolbar() {
        super.initToolbar();
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setNavigationDrawer("Restaurant");
    }

    private void selectFragment(Fragment fragment) {
        FragmentTransaction fragmentTransaction;
        fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.content_frame, fragment);
        fragmentTransaction.commit();
        mDrawerLayout.closeDrawers();
    }

    View.OnClickListener menuListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case (R.id.menu_home):
                    setNavigationTitle(R.string.menu_home);
                    selectFragment(new HalamanUtamaFragment());
                    break;
                case (R.id.menu_history):
                    setNavigationTitle(R.string.menu_history);
                    selectFragment(new CatatanTransaksiFragment());
                    break;
                case (R.id.menu_tentang):
                    setNavigationTitle(R.string.menu_tentang);
                    selectFragment(new TentangFragment());
                    break;
                case (R.id.menu_keluar):
                    profileHelper.setIdUser(null);
                    profileHelper.setSaldo(null);
                    profileHelper.setTanggalLahir(null);
                    profileHelper.setTempatLahir(null);
                    profileHelper.setNamaUser(null);
                    profileHelper.setEmail(null);
                    profileHelper.setFotoUrl(null);
                    Intent i = new Intent(MainActivity.this, LoginActivity.class);
                    startActivity(i);
                    finish();
                    break;
                case (R.id.navigation_header):
                    Intent intent = new Intent(MainActivity.this, ProfileActivity.class);
                    startActivity(intent);
                    break;
            }
        }
    };

    private void initMenu() {
        findViewById(R.id.menu_history).setOnClickListener(menuListener);
        findViewById(R.id.menu_keluar).setOnClickListener(menuListener);
        findViewById(R.id.menu_tentang).setOnClickListener(menuListener);
        findViewById(R.id.menu_home).setOnClickListener(menuListener);
        findViewById(R.id.navigation_header).setOnClickListener(menuListener);
    }

    private void initDrawer() {
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerToggle = new ActionBarDrawerToggle(
                this,
                mDrawerLayout,
                toolbar,
                R.string.drawer_open,
                R.string.drawer_close
        );
        initMenu();
    }

    private void initHeader() {
        imgHeader = (ImageView) findViewById(R.id.header_image);
        txtNameHeader = (TextView) findViewById(R.id.header_name);
        txtSaldo = (TextView) findViewById(R.id.header_saldo);
        txtLihatProfil = (TextView) findViewById(R.id.header_lp);
        txtNameHeader.setText(profileHelper.getNamaUser());
        txtSaldo.setText("Rp " + profileHelper.getSaldo());
    }

}
