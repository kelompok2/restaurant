package com.kelompok2.customer.ui.activity;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;

import com.kelompok2.customer.R;

/**
 * Created by Chinta Kumala on 02/04/2016.
 */
public class ProfileActivity extends NavigationActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
    }

    @Override
    public void initToolbar() {
        super.initToolbar();
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setNavigationBack(this, "Profil");
    }
}
