package com.kelompok2.customer.ui.activity;

import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.kelompok2.customer.R;
import com.kelompok2.customer.data.Registrasi;
import com.kelompok2.customer.koneksi.Filter;
import com.restaurant.connection.ServiceResponse;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Chinta Kumala on 29/03/2016.
 */
public class RegistrasiActivity extends NavigationActivity {

    private EditText noKtp;
    private EditText email;
    private EditText nama;
    private EditText password;
    private EditText konfPassword;
    private Button buttonDaftar;
    ImageView imageView;

    Registrasi registrasiData;
    BroadcastReceiver receiver = getReceiver();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registrasi);

        imageView = (ImageView) findViewById(R.id.img_backgoundRegis);
        Glide.with(this).load(R.drawable.bg_pict).crossFade().into(imageView);

        noKtp = (EditText) findViewById(R.id.regis_ktp);
        email = (EditText) findViewById(R.id.regis_email);
        nama = (EditText) findViewById(R.id.regis_nama);
        password = (EditText) findViewById(R.id.regis_pass);
        konfPassword = (EditText) findViewById(R.id.regis_konfpass);
        buttonDaftar = (Button) findViewById(R.id.regis_button);
        buttonDaftar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (password.getText().toString().equals(konfPassword.getText().toString())) {
                    registrasiData = new Registrasi();
                    registrasiData.email = email.getText().toString();
                    registrasiData.password = password.getText().toString();
                    registrasiData.konfPassword = konfPassword.getText().toString();
                    registrasiData.noKtp = noKtp.getText().toString();
                    registrasiData.nama = nama.getText().toString();

                    registrasi(registrasiData.toString());
                } else {
                    Toast.makeText(RegistrasiActivity.this, "Password tidak sesuai", Toast.LENGTH_LONG).show();
                }
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(receiver, new IntentFilter(Filter.CUSTOMER_REGISTRASI));
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(receiver);
    }

    @Override
    public void successResponse(ServiceResponse response, String filter) {
        super.successResponse(response, filter);
        Toast.makeText(this, "Registrasi berhasil", Toast.LENGTH_SHORT).show();

        Intent i = new Intent(RegistrasiActivity.this, LoginActivity.class);
        startActivity(i);
        finish();
    }

    @Override
    public void badResponse(ServiceResponse response, String filter) {
        super.badResponse(response, filter);
        try {
            JSONObject jsonObject = new JSONObject(response.getContent());
            String message = jsonObject.getString("message");
            Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
        } catch (JSONException e) {
            Log.e("Error JSON", "" + e);
        }
    }
}
