package com.kelompok2.customer.ui.activity;

import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.kelompok2.customer.R;
import com.kelompok2.customer.data.Login;
import com.kelompok2.customer.data.ProfileHelper;
import com.kelompok2.customer.koneksi.Filter;
import com.restaurant.connection.ServiceResponse;

import org.json.JSONException;
import org.json.JSONObject;

public class LoginActivity extends NavigationActivity {
    private EditText email;
    private EditText password;
    private Button button_login;

    ImageView imageView;

    ProfileHelper profileHelper;
    Login loginData;
    BroadcastReceiver receiver = getReceiver();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        imageView = (ImageView) findViewById(R.id.img_backgroundLogin);
        profileHelper = ProfileHelper.getInstance(this);
        email = (EditText) findViewById(R.id.login_email);
        password = (EditText) findViewById(R.id.login_pass);
        button_login = (Button) findViewById(R.id.login);
        button_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                loginData = new Login();
                loginData.email = email.getText().toString();
                loginData.password = password.getText().toString();

                login(loginData.toString());
            }
        });

        Glide.with(this).load(R.drawable.bg_pict).crossFade().into(imageView);
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(receiver, new IntentFilter(Filter.CUSTOMER_LOGIN));
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(receiver);

    }

    @Override
    public void successResponse(ServiceResponse response, String filter) {
        super.successResponse(response, filter);
        try {
            // Mengambil JSONObject keseluruhan
            JSONObject jsonParent = new JSONObject(response.getContent());
            Log.i(Filter.CUSTOMER_LOGIN, jsonParent.toString());

            // Mengambil JSONObject user
            JSONObject jsonUser = jsonParent.getJSONObject("user");
            String idUser = jsonUser.getString("id_user");
            String email = jsonUser.getString("email");
            String nama = jsonUser.getString("nama");
            String tempatLahir = jsonUser.getString("tempat_lahir");
            String tanggalLahir = jsonUser.getString("tanggal_lahir");
            String fotoUrl = jsonUser.getString("foto");
            String saldo = jsonUser.getString("saldo");

            // Menyimpan kedalam shared preference, atau semacam cache,
            // supaya bisa auto login (tidak perlu login setiap kali buka aplikasi)
            profileHelper.setIdUser(idUser);
            profileHelper.setEmail(email);
            profileHelper.setNamaUser(nama);
            profileHelper.setTempatLahir(tempatLahir);
            profileHelper.setTanggalLahir(tanggalLahir);
            profileHelper.setFotoUrl(fotoUrl);
            profileHelper.setSaldo(saldo);

            Intent i = new Intent(LoginActivity.this, MainActivity.class);
            startActivity(i);
            finish();
        } catch (JSONException e) {
            Log.e("LoginActivity", "" + e);
        }
    }

    @Override
    public void badResponse(ServiceResponse response, String filter) {
        super.badResponse(response, filter);
        String content = response.getContent();
        Log.e(Filter.CUSTOMER_LOGIN, content);
    }
}
