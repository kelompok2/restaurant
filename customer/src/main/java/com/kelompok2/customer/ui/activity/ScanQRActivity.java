package com.kelompok2.customer.ui.activity;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Point;
import android.graphics.Rect;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Display;
import android.view.View;
import android.view.Window;
import android.widget.RelativeLayout;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.Result;
import com.kelompok2.customer.R;

import java.util.Collections;

import me.dm7.barcodescanner.zxing.ZXingScannerView;

/**
 * Created by Chinta Kumala on 15/04/2016.
 */
public class ScanQRActivity extends NavigationActivity implements ZXingScannerView.ResultHandler {
    private ZXingScannerView scannerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan_qr);
        initToolbar();
        scannerView = (ZXingScannerView) findViewById(R.id.cameraPreview);
        scannerView.setFormats(Collections.singletonList(BarcodeFormat.QR_CODE));
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        Display display = getWindowManager().getDefaultDisplay();
        int width, height;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            Point point = new Point();
            display.getSize(point);
            width = point.x;
            height = point.y;
        } else {
            width = display.getWidth();
            height = display.getHeight();
        }

        Window window = getWindow();
        Rect rectangle = new Rect();
        window.getDecorView().getWindowVisibleDisplayFrame(rectangle);
        View content = window.findViewById(Window.ID_ANDROID_CONTENT);

        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(width, height);
        layoutParams.topMargin = -(content.getTop() - rectangle.top);
        layoutParams.bottomMargin = -(content.getBottom() - rectangle.bottom);
        scannerView.setLayoutParams(layoutParams);

    }

    @Override
    public void initToolbar() {
        super.initToolbar();
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setNavigationBack(this, "Scan QR Menu");
    }

    @Override
    public void handleResult(Result result) {
        Intent intent = new Intent();
        intent.putExtra("idMenu", result.toString());
        setResult(RESULT_OK, intent);
        finish();
    }

    @Override
    public void onResume() {
        super.onResume();
        scannerView.setResultHandler(this);
        scannerView.startCamera();
    }

    @Override
    public void onPause() {
        super.onPause();
        scannerView.stopCamera();
    }

}
