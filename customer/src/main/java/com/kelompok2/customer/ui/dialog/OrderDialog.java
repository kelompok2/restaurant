package com.kelompok2.customer.ui.dialog;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.kelompok2.customer.R;

/**
 * Created by Chinta Kumala on 14/04/2016.
 */
public class OrderDialog extends DialogFragment {
    private OnMessageClosed messageClosed;

    public static OrderDialog newIntance() {
        OrderDialog mejaDialog = new OrderDialog();
        return mejaDialog;
    }

    public void setOnMessageClosed(OnMessageClosed messageClosed) {
        this.messageClosed = messageClosed;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_jumlah_menu, container, false);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NO_TITLE, R.style.CustomDialog);
    }

    @Override
    public void onViewCreated(View v, @Nullable Bundle savedInstanceState) {
        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
        setStyle(STYLE_NO_FRAME, 0);
        final TextView txtJumlah = (TextView) v.findViewById(R.id.dialog_order_jumlah);
        Button btnBack = (Button) v.findViewById(R.id.dialog_order_kembali);
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (messageClosed != null) {
                    messageClosed.onClosed();
                }
                dismiss();
            }
        });

        Button btnLanjut = (Button) v.findViewById(R.id.dialog_order_lanjut);
        btnLanjut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (messageClosed != null) {
                    messageClosed.onProcess(txtJumlah.getText().toString());
                }
                dismiss();
            }
        });

    }

    public interface OnMessageClosed {
        public void onClosed();
        public void onProcess(String jumlah);
    }
}
