package com.kelompok2.customer.ui.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;
import com.kelompok2.customer.R;
import com.kelompok2.customer.koneksi.ClientServiceFragment;
import com.kelompok2.customer.ui.activity.MejaActivity;
import com.kelompok2.customer.ui.activity.MenuActivity;

/**
 * Created by Chinta Kumala on 02/04/2016.
 */
public class HalamanUtamaFragment extends ClientServiceFragment {
    ImageView imageView;
    ImageView imageButton1;
    ImageView imageButton2;

    RelativeLayout buttonMenu;
    RelativeLayout buttonMeja;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_halaman_utama, container, false);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        buttonMeja = (RelativeLayout) getActivity().findViewById(R.id.home_table);
        buttonMenu = (RelativeLayout) getActivity().findViewById(R.id.home_menu);
        imageView = (ImageView) getActivity().findViewById(R.id.menu_header_image);
        imageButton1 = (ImageView) getActivity().findViewById(R.id.menu_button_image1);
        imageButton2 = (ImageView) getActivity().findViewById(R.id.menu_button_image2);

        Glide.with(getActivity()).load(R.drawable.bg_menu).crossFade().into(imageButton1);
        Glide.with(getActivity()).load(R.drawable.bg_menu).crossFade().into(imageButton2);
        Glide.with(getActivity().getApplicationContext()).load(R.drawable.bg_home).crossFade().into(imageView);

        buttonMeja.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), MejaActivity.class);
                startActivity(intent);
            }
        });

        buttonMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), MenuActivity.class);
                startActivity(intent);
            }
        });
    }
}
