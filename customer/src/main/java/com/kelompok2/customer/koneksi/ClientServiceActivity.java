package com.kelompok2.customer.koneksi;

import com.restaurant.connection.Service;
import com.restaurant.connection.ServiceActivity;

/**
 * Created by User on 3/28/2016.
 */
public class ClientServiceActivity extends ServiceActivity {
    public void login(String parameter) {
        sendPostRequest(Path.BASE_URL + Path.CUSTOMER_LOGIN, parameter, Filter.CUSTOMER_LOGIN, Service.CONTENT_TYPE_URLENCODED);

    }

    public void registrasi(String parameter) {
        sendPostRequest(Path.BASE_URL + Path.CUSTOMER_REGISTRASI, parameter, Filter.CUSTOMER_REGISTRASI, Service.CONTENT_TYPE_URLENCODED);
    }

    public void pilihMeja(String parameter) {
        sendPostRequest(Path.BASE_URL + Path.CUSTOMER_PILIH_MEJA, parameter, Filter.CUSTOMER_PILIH_MEJA, Service.CONTENT_TYPE_URLENCODED);
    }

    public void pesanMenu(String parameter) {
        sendPostRequest(Path.BASE_URL + Path.CUSTOMER_PESAN_MENU, parameter, Filter.CUSTOMER_PESAN_MENU, Service.CONTENT_TYPE_URLENCODED);
    }

    public void getMeja(){
        sendGetRequest(Path.BASE_URL + Path.CUSTOMER_MEJA, Filter.CUSTOMER_MEJA);
    }

    public void getMenu() {
        sendGetRequest(Path.BASE_URL + Path.CUSTOMER_MENU, Filter.CUSTOMER_MENU);
    }
}

