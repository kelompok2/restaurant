package com.kelompok2.customer.koneksi;

/**
 * Created by User on 3/28/2016.
 */
public class Path {
    public static final String BASE_URL = "http://www.tubes.pe.hu/api/";

    public static final String CUSTOMER_LOGIN = "customer/login.php";
    public static final String CUSTOMER_REGISTRASI = "customer/registrasi.php";
    public static final String CUSTOMER_MEJA = "customer/meja.php";
    public static final String CUSTOMER_MENU = "customer/menu.php";
    public static final String CUSTOMER_PILIH_MEJA = "customer/pilihmeja.php";
    public static final String CUSTOMER_PESAN_MENU = "customer/pesanmenu.php";
}
