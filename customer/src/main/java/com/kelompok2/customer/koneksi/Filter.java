package com.kelompok2.customer.koneksi;

/**
 * Created by User on 3/28/2016.
 */
public class Filter {
    // Kelas ini digunakan filter hasil nembak ke server
    public static final String CUSTOMER_LOGIN = "customerLogin";
    public static final String CUSTOMER_REGISTRASI = "customerRegistrasi";
    public static final String CUSTOMER_MEJA = "customerMeja";
    public static final String CUSTOMER_MENU = "customerMenu";
    public static final String CUSTOMER_PILIH_MEJA = "customerPilihMeja";
    public static final String CUSTOMER_PESAN_MENU = "customerPesanMenu";

}
