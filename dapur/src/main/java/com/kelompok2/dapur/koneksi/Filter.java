package com.kelompok2.dapur.koneksi;

/**
 * Created by User on 3/28/2016.
 */
public class Filter {
    public static final String LOGIN = "merchantLogin";
    public static final String ORDERS = "merchantOrders";
    public static final String UPDATE_STATUS = "merchantUpdateStatus";
    public static final String TAMBAH_MENU = "merchantTambahMenu";
}
