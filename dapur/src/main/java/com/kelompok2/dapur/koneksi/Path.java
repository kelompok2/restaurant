package com.kelompok2.dapur.koneksi;

/**
 * Created by User on 3/28/2016.
 */
public class Path {
    public static final String BASE_URL = "http://www.tubes.pe.hu/api/";

    public static final String LOGIN = "merchant/login.php";
    public static final String ORDERS = "merchant/orders.php";
    public static final String UPDATE_STATUS = "merchant/updateorder.php";
    public static final String TAMBAH_MENU = "merchant/tambahmenu.php";
}
