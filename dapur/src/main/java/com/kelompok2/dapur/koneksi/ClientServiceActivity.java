package com.kelompok2.dapur.koneksi;

import com.restaurant.connection.Service;
import com.restaurant.connection.ServiceActivity;

/**
 * Created by User on 3/28/2016.
 */
public class ClientServiceActivity extends ServiceActivity {
    public void login(String parameter) {
        requestPost(parameter, Path.LOGIN, Filter.LOGIN);
    }

    public void getOrders() {
        requestGet(Path.ORDERS, Filter.ORDERS);
    }

    public void updateStatusOrder(String paramater) {
        requestPost(paramater, Path.UPDATE_STATUS, Filter.UPDATE_STATUS);
    }

    public void tambahMenu(String parameter) {
        requestPost(parameter, Path.TAMBAH_MENU, Filter.TAMBAH_MENU);
    }

    private void requestPost(String parameter, String path, String filter) {
        sendPostRequest(Path.BASE_URL + path, parameter, filter, Service.CONTENT_TYPE_URLENCODED);
    }

    private void requestGet(String path, String filter) {
        sendGetRequest(Path.BASE_URL + path, filter);
    }
}
