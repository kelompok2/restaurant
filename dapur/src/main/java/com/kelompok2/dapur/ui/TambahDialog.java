package com.kelompok2.dapur.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import com.kelompok2.dapur.R;
import com.kelompok2.dapur.data.Menu;

/**
 * Created by Chinta Kumala on 14/04/2016.
 */
public class TambahDialog extends DialogFragment {
    private OnMessageClosed messageClosed;

    public static TambahDialog newIntance() {
        TambahDialog statusDialog = new TambahDialog();
        return statusDialog;
    }

    public void setOnMessageClosed(OnMessageClosed messageClosed) {
        this.messageClosed = messageClosed;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_tambah_menu, container, false);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NO_TITLE, R.style.CustomDialog);
    }

    @Override
    public void onViewCreated(View v, @Nullable Bundle savedInstanceState) {
        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
        setStyle(STYLE_NO_FRAME, 0);
        final TextView txtNama = (TextView) v.findViewById(R.id.dialog_tambah_nama);
        final TextView txtHarga = (TextView) v.findViewById(R.id.dialog_tambah_harga);
        final Spinner spnJenis = (Spinner) v.findViewById(R.id.dialog_tambah_jenis);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity(), R.array.jenis_array, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spnJenis.setAdapter(adapter);

        Button btn = (Button) v.findViewById(R.id.dialog_tambah_btn);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (messageClosed != null) {
                    Menu menu = new Menu();
                    menu.setNamaMenu(txtNama.getText().toString());
                    menu.setHarga(txtHarga.getText().toString());
                    if ((spnJenis.getSelectedItem()).equals("Makanan")) {
                        menu.setIdJenis("MKN");
                        String idmenu = "MKN_" + txtNama.getText().toString();
                        menu.setIdMenu(idmenu);
                    } else {
                        menu.setIdJenis("MNM");
                        String idmenu = "MNM_" + txtNama.getText().toString();
                        menu.setIdMenu(idmenu);
                    }
                    messageClosed.onClosed(menu);
                }
                dismiss();
            }
        });
    }

    public interface OnMessageClosed {
        public void onClosed(Menu menu);
    }
}
