package com.kelompok2.dapur.ui;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.kelompok2.dapur.R;
import com.kelompok2.dapur.koneksi.ClientServiceActivity;
import com.kelompok2.dapur.util.Util;

/**
 * Created by User on 3/2/2016.
 */
public class NavigationActivity extends ClientServiceActivity {
    public Toolbar toolbar;
    public ProgressDialog progressDialog;

    public void showProgressMessage(Context context, String message) {
        progressDialog = Util.createProgressDialog(context, message);
        progressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {}
        });
        progressDialog.show();
    }

    public void dismissProgressMessage() {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
    }

    public void setNavigationTitle(int title) {
        toolbar.setTitle(title);
    }

    public void setNavigationTitle(String title) {
        toolbar.setTitle(title);
    }

    public void setNavigationDrawer(int title, View.OnClickListener listener) {
        toolbar.setTitle(title);
        toolbar.setNavigationIcon(R.drawable.ic_menu_white_24dp);
        toolbar.setNavigationOnClickListener(listener);
    }

    public void setNavigationDrawer(String title) {
        toolbar.setTitle(title);
        toolbar.setNavigationIcon(R.drawable.ic_menu_white_24dp);
    }

    public void setNavigationDrawer(int title) {
        toolbar.setTitle(title);
        toolbar.setNavigationIcon(R.drawable.ic_menu_white_24dp);
    }

    public void setNavigationBack(final Activity activity, String title) {
        toolbar.setTitle(title);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.finish();
            }
        });
    }

    public void setNavigationBack(final Activity activity, int title) {
        toolbar.setTitle(title);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.finish();
            }
        });
    }

    public void initToolbar(){}

}