package com.kelompok2.dapur.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import com.kelompok2.dapur.R;

/**
 * Created by Chinta Kumala on 14/04/2016.
 */
public class StatusDialog extends DialogFragment {
    private OnMessageClosed messageClosed;

    public static StatusDialog newIntance(String menu, String jumlah) {
        StatusDialog statusDialog = new StatusDialog();
        Bundle bundle = new Bundle();
        bundle.putCharSequence("nama_menu", menu);
        bundle.putCharSequence("jumlah", jumlah);
        statusDialog.setArguments(bundle);
        return statusDialog;
    }

    public void setOnMessageClosed(OnMessageClosed messageClosed) {
        this.messageClosed = messageClosed;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_status, container, false);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NO_TITLE, R.style.CustomDialog);
    }

    @Override
    public void onViewCreated(View v, @Nullable Bundle savedInstanceState) {
        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
        setStyle(STYLE_NO_FRAME, 0);
        TextView txtNama = (TextView) v.findViewById(R.id.dialog_status_menu);
        txtNama.setText(getArguments().getCharSequence("nama_menu") + " - " + getArguments().getCharSequence("jumlah"));
        final Spinner spnStatus = (Spinner) v.findViewById(R.id.dialog_status_status);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity(), R.array.status_array, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spnStatus.setAdapter(adapter);
        /*txtType.setOnItemSelectedListener(itemListener);*/

        Button btnUpdate = (Button) v.findViewById(R.id.dialog_status_update);
        btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (messageClosed != null) {
                    messageClosed.onClosed((String) spnStatus.getSelectedItem());
                }
                dismiss();
            }
        });
    }

    public interface OnMessageClosed {
        public void onClosed(String status);
    }
}
