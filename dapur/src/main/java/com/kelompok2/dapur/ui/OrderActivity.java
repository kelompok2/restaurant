package com.kelompok2.dapur.ui;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.kelompok2.dapur.R;
import com.kelompok2.dapur.data.Orders;
import com.kelompok2.dapur.koneksi.Filter;
import com.kelompok2.dapur.util.Util;
import com.restaurant.connection.ServiceResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Chinta Kumala on 16/04/2016.
 */
public class OrderActivity extends NavigationActivity {

    RecyclerView recyclerView;
    GridLayoutManager manager;
    BroadcastReceiver receiver = getReceiver();
    OrdersAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order);
        initToolbar();

        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        manager = new GridLayoutManager(OrderActivity.this, 1);
        recyclerView.setLayoutManager(manager);
        recyclerView.setAdapter(adapter = new OrdersAdapter(OrderActivity.this));
        manager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                return position == 0 ? manager.getSpanCount() : 1;
            }
        });
        recyclerView.setHasFixedSize(false);
        showProgressMessage(this, "Harap tunggu");
        getOrders();
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(receiver, new IntentFilter(Filter.ORDERS));
        registerReceiver(receiver, new IntentFilter(Filter.UPDATE_STATUS));
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(receiver);
    }

    @Override
    public void successResponse(ServiceResponse response, String filter) {
        super.successResponse(response, filter);
        dismissProgressMessage();
        Log.i("Order", response.getContent());
        switch (filter) {
            case Filter.ORDERS:
                try {
                    JSONObject jsonObject = new JSONObject(response.getContent());
                    JSONArray jsonArray = jsonObject.getJSONArray("daftar_order");
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject json = jsonArray.getJSONObject(i);
                        Orders orders = new Orders();
                        orders.setIdOrder(json.getString("id_order"));
                        orders.setIdMenu(json.getString("id_menu"));
                        orders.setNamaMenu(json.getString("nama_menu"));
                        orders.setStatus(json.getString("status"));
                        orders.setJumlah(json.getString("jumlah"));
                        adapter.addOrder(orders);
                    }
                } catch (JSONException e) {
                    Log.e("OrderActivity", "" + e);
                }
                break;
            case Filter.UPDATE_STATUS:
                Toast.makeText(this, response.getContent(), Toast.LENGTH_LONG).show();
//                showProgressMessage(this, "Memperbarui pesanan");
//                adapter = new OrdersAdapter(OrderActivity.this);
//                getOrders();
                finish();
                break;
            default:
                break;
        }

    }

    @Override
    public void badResponse(ServiceResponse response, String filter) {
        super.badResponse(response, filter);
        dismissProgressMessage();
        Toast.makeText(this, response.getMessage(), Toast.LENGTH_LONG).show();
    }

    @Override
    public void initToolbar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setNavigationBack(this, "Daftar Pesanan");
    }

    class OrdersAdapter extends RecyclerView.Adapter {
        private ArrayList<Orders> orderses;
        private Context context;

        public OrdersAdapter(Context context) {
            this.context = context;
            orderses = new ArrayList<>();
        }

        public void addOrder(Orders order) {
            orderses.add(order);
            notifyDataSetChanged();
        }

        public Orders getItem(int position) {
            return orderses.get(position);
        }



        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_order,
                    parent, false);
            return new Holder(v);
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            ((Holder) holder).setValue(orderses.get(position));
        }

        @Override
        public int getItemCount() {
            return orderses.size();
        }
    }

    private class Holder extends RecyclerView.ViewHolder {
        TextView txtNama;
        TextView txtStatus;

        public Holder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(itemListener);
            txtNama = (TextView) itemView.findViewById(R.id.order_nama);
            txtStatus = (TextView) itemView.findViewById(R.id.order_status);
        }

        public void setValue(Orders menu) {
            txtNama.setText(menu.getNamaMenu());
            txtStatus.setText(menu.getJumlah() + " - " + menu.getStatus());
        }

    }

    View.OnClickListener itemListener = new View.OnClickListener() {
        @Override
        public void onClick(final View v) {
            Orders orders = adapter.getItem(recyclerView.getChildAdapterPosition(v));
            Util.showStatusDialog(getSupportFragmentManager(), orders.getNamaMenu(), orders.getJumlah()).setOnMessageClosed(new StatusDialog.OnMessageClosed() {
                @Override
                public void onClosed(String status) {
                    Orders order = adapter.getItem(recyclerView.getChildAdapterPosition(v));
                    order.setStatus(status);
                    Log.i("parameter", order.toString());
                    showProgressMessage(OrderActivity.this, "Harap tunggu");
                    updateStatusOrder(order.toString());
                }
            });
        }
    };
}
