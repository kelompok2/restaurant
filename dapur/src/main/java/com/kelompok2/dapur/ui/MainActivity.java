package com.kelompok2.dapur.ui;

import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.kelompok2.dapur.R;
import com.kelompok2.dapur.data.Menu;
import com.kelompok2.dapur.koneksi.Filter;
import com.kelompok2.dapur.util.Util;
import com.restaurant.connection.ServiceResponse;

/**
 * Created by Chinta Kumala on 16/04/2016.
 */
public class MainActivity extends NavigationActivity {

    LinearLayout btnOrder;
    LinearLayout btnMenu;
    LinearLayout btnExit;

    BroadcastReceiver receiver = getReceiver();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btnOrder = (LinearLayout) findViewById(R.id.menu_order);
        btnOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, OrderActivity.class);
                startActivity(intent);
            }
        });
        btnMenu = (LinearLayout) findViewById(R.id.menu_tambah);
        btnMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Util.showTambahDialog(getSupportFragmentManager()).setOnMessageClosed(new TambahDialog.OnMessageClosed() {
                    @Override
                    public void onClosed(Menu menu) {
                        showProgressMessage(MainActivity.this, "Harap tunggu");
                        tambahMenu(menu.toString());
                    }
                });

            }
        });
        btnExit = (LinearLayout) findViewById(R.id.menu_exit);
        btnExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
            }
        });

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setNavigationTitle("Halaman Utama");
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(receiver, new IntentFilter(Filter.TAMBAH_MENU));
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(receiver);
    }

    @Override
    public void successResponse(ServiceResponse response, String filter) {
        super.successResponse(response, filter);
        dismissProgressMessage();
        Toast.makeText(this, response.getContent(), Toast.LENGTH_LONG).show();
    }

    @Override
    public void badResponse(ServiceResponse response, String filter) {
        super.badResponse(response, filter);
        dismissProgressMessage();
        Toast.makeText(this, response.getContent(), Toast.LENGTH_LONG).show();
    }
}
