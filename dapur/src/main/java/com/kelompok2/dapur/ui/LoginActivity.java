package com.kelompok2.dapur.ui;

import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.kelompok2.dapur.R;
import com.kelompok2.dapur.data.Login;
import com.kelompok2.dapur.koneksi.Filter;
import com.restaurant.connection.ServiceResponse;

/**
 * Created by user on 4/5/2016.
 */
public class LoginActivity extends NavigationActivity {

    Button btnLogin;
    EditText txtUser;
    EditText txtPass;
    ImageView imgBackground;
    Login loginData;

    BroadcastReceiver receiver = getReceiver();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        initComponents();
    }

    // kita bikin method sendiri buat khusus nampung inisialisasi semua komponen yang mau dipake
    private void initComponents() {
        btnLogin = (Button) findViewById(R.id.loginBtnLogin);
        txtUser = (EditText) findViewById(R.id.loginTxtUser);
        txtPass = (EditText) findViewById(R.id.loginTxtPass);
        imgBackground = (ImageView) findViewById(R.id.loginImgBackground);

        // caai manggil glidenya gini...cuma sebaris...
        // Glide.with(Context).load(drawable_gambar atau link).into(ImageView nya);
        Glide.with(this).load(R.drawable.chef).into(imgBackground);

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loginData = new Login();
                loginData.username = txtUser.getText().toString();
                loginData.password = txtPass.getText().toString();
                showProgressMessage(LoginActivity.this, "Harap tunggu");
                login(loginData.toString());
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(receiver, new IntentFilter(Filter.LOGIN));
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(receiver);
    }

    @Override
    public void successResponse(ServiceResponse response, String filter) {
        super.successResponse(response, filter);
        dismissProgressMessage();
        Log.i("LoginActivity", response.getContent());
        /*Toast.makeText(this, response.getContent(), Toast.LENGTH_LONG).show();*/
        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void badResponse(ServiceResponse response, String filter) {
        super.badResponse(response, filter);
        dismissProgressMessage();
        Log.e("LoginActivity", response.getContent());
        Toast.makeText(this, response.getContent(), Toast.LENGTH_LONG).show();
    }
}
