package com.kelompok2.dapur.data;

import android.net.Uri;

import java.io.Serializable;

/**
 * Created by Chinta Kumala on 16/04/2016.
 */
public class Menu implements Serializable {
    private String idMenu;
    private String namaMenu;
    private String harga;
    private String idJenis;

    public String getIdMenu() {
        return idMenu;
    }

    public void setIdMenu(String idMenu) {
        this.idMenu = idMenu;
    }

    public String getNamaMenu() {
        return namaMenu;
    }

    public void setNamaMenu(String namaMenu) {
        this.namaMenu = namaMenu;
    }

    public String getHarga() {
        return harga;
    }

    public void setHarga(String harga) {
        this.harga = harga;
    }

    public String getIdJenis() {
        return idJenis;
    }

    public void setIdJenis(String idJenis) {
        this.idJenis = idJenis;
    }

    @Override
    public String toString() {
        String result;
        result = Uri.encode("id_menu=" + idMenu +
                "&id_jenis=" + idJenis +
                "&harga=" + harga +
                "&nama_menu=" + namaMenu);
        result=result.replaceAll("%20","+");
        result=result.replaceAll("%3D","=");
        result=result.replaceAll("%26","&");
        return result;
    }
}
