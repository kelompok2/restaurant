package com.kelompok2.dapur.data;

import android.net.Uri;

import java.io.Serializable;

/**
 * Created by Chinta Kumala on 16/04/2016.
 */
public class Orders implements Serializable {
    private String idMenu;
    private String namaMenu;
    private String jumlah;
    private String status;
    private String idOrder;

    public String getIdOrder() {
        return idOrder;
    }

    public void setIdOrder(String idOrder) {
        this.idOrder = idOrder;
    }

    public String getIdMenu() {
        return idMenu;
    }

    public void setIdMenu(String idMenu) {
        this.idMenu = idMenu;
    }

    public String getNamaMenu() {
        return namaMenu;
    }

    public void setNamaMenu(String namaMenu) {
        this.namaMenu = namaMenu;
    }

    public String getJumlah() {
        return jumlah;
    }

    public void setJumlah(String jumlah) {
        this.jumlah = jumlah;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        String result;
        result = Uri.encode("id_order=" + idOrder +
                "&id_menu=" + idMenu +
                "&status=" + status);
        result=result.replaceAll("%20","+");
        result=result.replaceAll("%3D","=");
        result=result.replaceAll("%26","&");
        return result;
    }
}
