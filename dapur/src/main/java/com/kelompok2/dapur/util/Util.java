package com.kelompok2.dapur.util;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Build;
import android.support.v4.app.FragmentManager;
import android.view.Window;

import com.kelompok2.dapur.ui.StatusDialog;
import com.kelompok2.dapur.ui.TambahDialog;

/**
 * Created by Chinta Kumala on 16/04/2016.
 */
public class Util {
    public static StatusDialog showStatusDialog(FragmentManager fragmentManager, String nama_menu, String jumlah) {
        StatusDialog dialog = StatusDialog.newIntance(nama_menu, jumlah);
        dialog.show(fragmentManager, "dialogMessage");
        return dialog;
    }

    public static TambahDialog showTambahDialog(FragmentManager fragmentManager) {
        TambahDialog dialog = TambahDialog.newIntance();
        dialog.show(fragmentManager, "dialogMessage");
        return dialog;
    }

    public static ProgressDialog createProgressDialog(Context context, String message) {
        ProgressDialog dialog = new ProgressDialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        }
        dialog.setMessage(message);
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
        return dialog;
    }

}
